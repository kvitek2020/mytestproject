# --- !Ups

create table entities
(
    name varchar,
    type varchar,
    css varchar,
    id_parameters int
);

create table parameters
(
    id int,
    labelText varchar,
    parameters varchar
);

alter table ENTITIES
    add constraint ENTITIES_PARAMETERS_ID_fk
        foreign key (ID_PARAMETERS) references PARAMETERS (ID);

create unique index entities_name_uindex
    on entities (name);

insert into parameters
    (id, labelText, parameters) values (2, 'О Себе', '');
insert into entities
    (name, type, css, id_parameters) values ( 'about_me', 'textarea', 'width:300px&height:75px&margin:0px', 2);

insert into parameters
    (id, labelText, parameters) values (3, 'Введите имя', 'text');
insert into entities
    (name, type, css, id_parameters) values ( 'name', 'input', 'width:150px', 3);

insert into parameters
    (id, labelText, parameters) values (4, 'Введите Отчество', 'text');
insert into entities
    (name, type, css, id_parameters) values ( 'secname', 'input', 'width:150px', 4);

insert into parameters
    (id, labelText, parameters) values (5, '', 'submit');
insert into entities
    (name, type, css, id_parameters) values ( 'submit', 'input', '', 5);

insert into parameters
    (id, labelText, parameters) values (6, '', 'ТЕСТ1&ТЕСТ2&ТЕСТ3&TEST4');
insert into entities
    (name, type, css, id_parameters) values ( '', 'select', '', 6);

# --- !Downs

drop table entities;
drop table parameters;