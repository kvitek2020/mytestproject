package facades;

import models.Entities;
import services.ebean.EbeanDefaultServer;

import javax.inject.Inject;
import java.util.List;

public class EntitiesFacade {

    @Inject
    EbeanDefaultServer server;

    public EntitiesFacade() {
    }
    public List<Entities> getAllEntities() {
        return server.createQuery(Entities.class)
                .fetch("parameters", "*")
                .select("*")
                .findList();
    }
}
