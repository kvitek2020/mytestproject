package views;
import models.Entities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Controller;
import play.twirl.api.Html;


public class ObjectBuilder extends Controller {

    private static int id = 1;

    private static final Logger LOG= LoggerFactory.getLogger(ObjectBuilder.class);

    public static final String label = "label";
    public static final String textarea = "textarea";
    public static final String input = "input";
    public static final String select = "select";
    public static final String checkbox = "checkbox";

    public static String getId() {
        return Integer.toString(id++);
    }

    public static Html createObject(Entities entities) throws Exception {

        String css = entities.getCss().replaceAll("&", ";");
        String name = entities.getName().equals("")?entities.getName():Integer.toString(id++);
        String type = entities.getType();
        String parameters = entities.getParameters().getParameters();
        String labelText = entities.getParameters().getLabelText();

        LOG.debug("name = {}, type = {}, css = {}, parameters = {}, labelText = {}", name, type, css, parameters, labelText);
        switch (type){
            case ObjectBuilder.label:{
                return views.html.objects.label.render(parameters, css, "", name, labelText);
            }
            case ObjectBuilder.textarea:{
                return views.html.objects.textarea.render(css, parameters, name, labelText);
            }
            case ObjectBuilder.select:{
                return views.html.objects.select.render(parameters, name, labelText);
            }
            case ObjectBuilder.input:{
                return views.html.objects.input.render(css, parameters, name, labelText);
            }
            default:
                throw new Exception("Такой объект не существует");
        }
    }
}
