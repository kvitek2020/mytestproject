package services.ebean;

import io.ebean.*;
import io.ebean.annotation.TxIsolation;
import io.ebean.cache.ServerCacheManager;
import io.ebean.meta.MetaInfoManager;
import io.ebean.plugin.Property;
import io.ebean.plugin.SpiServer;
import io.ebean.text.csv.CsvReader;
import io.ebean.text.json.JsonContext;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class EbeanDefaultServer {
    EbeanServer defaultServer;
    public EbeanDefaultServer() {
        defaultServer = Ebean.getDefaultServer();
    }

    public void shutdown(boolean shutdownDataSource, boolean deregisterDriver) {
        defaultServer.shutdown(shutdownDataSource, deregisterDriver);
    }

    public AutoTune getAutoTune() {
        return defaultServer.getAutoTune();
    }

    public String getName() {
        return defaultServer.getName();
    }

    public ExpressionFactory getExpressionFactory() {
        return defaultServer.getExpressionFactory();
    }

    public MetaInfoManager getMetaInfoManager() {
        return defaultServer.getMetaInfoManager();
    }

    public SpiServer getPluginApi() {
        return defaultServer.getPluginApi();
    }

    public BeanState getBeanState(Object bean) {
        return defaultServer.getBeanState(bean);
    }

    public Object getBeanId(Object bean) {
        return defaultServer.getBeanId(bean);
    }

    public Object setBeanId(Object bean, Object id) {
        return defaultServer.setBeanId(bean, id);
    }

    public Map<String, ValuePair> diff(Object newBean, Object oldBean) {
        return defaultServer.diff(newBean, oldBean);
    }

    public <T> T createEntityBean(Class<T> type) {
        return defaultServer.createEntityBean(type);
    }

    public <T> CsvReader<T> createCsvReader(Class<T> beanType) {
        return defaultServer.createCsvReader(beanType);
    }

    public <T> UpdateQuery<T> update(Class<T> beanType) {
        return defaultServer.update(beanType);
    }

    public <T> Query<T> createNamedQuery(Class<T> beanType, String namedQuery) {
        return defaultServer.createNamedQuery(beanType, namedQuery);
    }

    public <T> Query<T> createQuery(Class<T> beanType) {
        return defaultServer.createQuery(beanType);
    }

    public <T> Query<T> createQuery(Class<T> beanType, String ormQuery) {
        return defaultServer.createQuery(beanType, ormQuery);
    }

    public <T> Query<T> find(Class<T> beanType) {
        return defaultServer.find(beanType);
    }

    public <T> Query<T> findNative(Class<T> beanType, String nativeSql) {
        return defaultServer.findNative(beanType, nativeSql);
    }

    public Object nextId(Class<?> beanType) {
        return defaultServer.nextId(beanType);
    }

    public <T> Filter<T> filter(Class<T> beanType) {
        return defaultServer.filter(beanType);
    }

    public <T> void sort(List<T> list, String sortByClause) {
        defaultServer.sort(list, sortByClause);
    }

    public <T> Update<T> createUpdate(Class<T> beanType, String ormUpdate) {
        return defaultServer.createUpdate(beanType, ormUpdate);
    }

    public <T> DtoQuery<T> findDto(Class<T> dtoType, String sql) {
        return defaultServer.findDto(dtoType, sql);
    }

    public <T> DtoQuery<T> createNamedDtoQuery(Class<T> dtoType, String namedQuery) {
        return defaultServer.createNamedDtoQuery(dtoType, namedQuery);
    }

    public SqlQuery createSqlQuery(String sql) {
        return defaultServer.createSqlQuery(sql);
    }

    public SqlUpdate createSqlUpdate(String sql) {
        return defaultServer.createSqlUpdate(sql);
    }

    public CallableSql createCallableSql(String callableSql) {
        return defaultServer.createCallableSql(callableSql);
    }

    public void register(TransactionCallback transactionCallback) throws PersistenceException {
        defaultServer.register(transactionCallback);
    }

    public Transaction createTransaction() {
        return defaultServer.createTransaction();
    }

    public Transaction createTransaction(TxIsolation isolation) {
        return defaultServer.createTransaction(isolation);
    }

    public Transaction beginTransaction() {
        return defaultServer.beginTransaction();
    }

    public Transaction beginTransaction(TxIsolation isolation) {
        return defaultServer.beginTransaction(isolation);
    }

    public Transaction beginTransaction(TxScope scope) {
        return defaultServer.beginTransaction(scope);
    }

    public Transaction currentTransaction() {
        return defaultServer.currentTransaction();
    }

    public void flush() {
        defaultServer.flush();
    }

    public void commitTransaction() {
        defaultServer.commitTransaction();
    }

    public void rollbackTransaction() {
        defaultServer.rollbackTransaction();
    }

    public void endTransaction() {
        defaultServer.endTransaction();
    }

    public void refresh(Object bean) {
        defaultServer.refresh(bean);
    }

    public void refreshMany(Object bean, String propertyName) {
        defaultServer.refreshMany(bean, propertyName);
    }

    @Nullable
    public <T> T find(Class<T> beanType, Object id) {
        return defaultServer.find(beanType, id);
    }

    @Nonnull
    public <T> T getReference(Class<T> beanType, Object id) {
        return defaultServer.getReference(beanType, id);
    }

    public <T> int findCount(Query<T> query, Transaction transaction) {
        return defaultServer.findCount(query, transaction);
    }

    @Nonnull
    public <A, T> List<A> findIds(Query<T> query, Transaction transaction) {
        return defaultServer.findIds(query, transaction);
    }

    @Nonnull
    public <T> QueryIterator<T> findIterate(Query<T> query, Transaction transaction) {
        return defaultServer.findIterate(query, transaction);
    }

    public <T> void findEach(Query<T> query, Consumer<T> consumer, Transaction transaction) {
        defaultServer.findEach(query, consumer, transaction);
    }

    public <T> void findEachWhile(Query<T> query, Predicate<T> consumer, Transaction transaction) {
        defaultServer.findEachWhile(query, consumer, transaction);
    }

    @Nonnull
    public <T> List<Version<T>> findVersions(Query<T> query, Transaction transaction) {
        return defaultServer.findVersions(query, transaction);
    }

    @Nonnull
    public <T> List<T> findList(Query<T> query, Transaction transaction) {
        return defaultServer.findList(query, transaction);
    }

    @Nonnull
    public <T> FutureRowCount<T> findFutureCount(Query<T> query, Transaction transaction) {
        return defaultServer.findFutureCount(query, transaction);
    }

    @Nonnull
    public <T> FutureIds<T> findFutureIds(Query<T> query, Transaction transaction) {
        return defaultServer.findFutureIds(query, transaction);
    }

    @Nonnull
    public <T> FutureList<T> findFutureList(Query<T> query, Transaction transaction) {
        return defaultServer.findFutureList(query, transaction);
    }

    @Nonnull
    public <T> PagedList<T> findPagedList(Query<T> query, Transaction transaction) {
        return defaultServer.findPagedList(query, transaction);
    }

    @Nonnull
    public <T> Set<T> findSet(Query<T> query, Transaction transaction) {
        return defaultServer.findSet(query, transaction);
    }

    @Nonnull
    public <K, T> Map<K, T> findMap(Query<T> query, Transaction transaction) {
        return defaultServer.findMap(query, transaction);
    }

    @Nonnull
    public <A, T> List<A> findSingleAttributeList(Query<T> query, Transaction transaction) {
        return defaultServer.findSingleAttributeList(query, transaction);
    }

    @Nullable
    public <T> T findOne(Query<T> query, Transaction transaction) {
        return defaultServer.findOne(query, transaction);
    }

    @Nonnull
    public <T> Optional<T> findOneOrEmpty(Query<T> query, Transaction transaction) {
        return defaultServer.findOneOrEmpty(query, transaction);
    }

    public <T> int delete(Query<T> query, Transaction transaction) {
        return defaultServer.delete(query, transaction);
    }

    public <T> int update(Query<T> query, Transaction transaction) {
        return defaultServer.update(query, transaction);
    }

    @Nonnull
    public List<SqlRow> findList(SqlQuery query, Transaction transaction) {
        return defaultServer.findList(query, transaction);
    }

    public void findEach(SqlQuery query, Consumer<SqlRow> consumer, Transaction transaction) {
        defaultServer.findEach(query, consumer, transaction);
    }

    public void findEachWhile(SqlQuery query, Predicate<SqlRow> consumer, Transaction transaction) {
        defaultServer.findEachWhile(query, consumer, transaction);
    }

    @Nullable
    public SqlRow findOne(SqlQuery query, Transaction transaction) {
        return defaultServer.findOne(query, transaction);
    }

    public void save(Object bean) throws OptimisticLockException {
        defaultServer.save(bean);
    }

    public int saveAll(Collection<?> beans) throws OptimisticLockException {
        return defaultServer.saveAll(beans);
    }

    public boolean delete(Object bean) throws OptimisticLockException {
        return defaultServer.delete(bean);
    }

    public boolean delete(Object bean, Transaction transaction) throws OptimisticLockException {
        return defaultServer.delete(bean, transaction);
    }

    public boolean deletePermanent(Object bean) throws OptimisticLockException {
        return defaultServer.deletePermanent(bean);
    }

    public boolean deletePermanent(Object bean, Transaction transaction) throws OptimisticLockException {
        return defaultServer.deletePermanent(bean, transaction);
    }

    public int deleteAllPermanent(Collection<?> beans) throws OptimisticLockException {
        return defaultServer.deleteAllPermanent(beans);
    }

    public int deleteAllPermanent(Collection<?> beans, Transaction transaction) throws OptimisticLockException {
        return defaultServer.deleteAllPermanent(beans, transaction);
    }

    public int delete(Class<?> beanType, Object id) {
        return defaultServer.delete(beanType, id);
    }

    public int delete(Class<?> beanType, Object id, Transaction transaction) {
        return defaultServer.delete(beanType, id, transaction);
    }

    public int deletePermanent(Class<?> beanType, Object id) {
        return defaultServer.deletePermanent(beanType, id);
    }

    public int deletePermanent(Class<?> beanType, Object id, Transaction transaction) {
        return defaultServer.deletePermanent(beanType, id, transaction);
    }

    public int deleteAll(Collection<?> beans) throws OptimisticLockException {
        return defaultServer.deleteAll(beans);
    }

    public int deleteAll(Collection<?> beans, Transaction transaction) throws OptimisticLockException {
        return defaultServer.deleteAll(beans, transaction);
    }

    public int deleteAll(Class<?> beanType, Collection<?> ids) {
        return defaultServer.deleteAll(beanType, ids);
    }

    public int deleteAll(Class<?> beanType, Collection<?> ids, Transaction transaction) {
        return defaultServer.deleteAll(beanType, ids, transaction);
    }

    public int deleteAllPermanent(Class<?> beanType, Collection<?> ids) {
        return defaultServer.deleteAllPermanent(beanType, ids);
    }

    public int deleteAllPermanent(Class<?> beanType, Collection<?> ids, Transaction transaction) {
        return defaultServer.deleteAllPermanent(beanType, ids, transaction);
    }

    public int execute(SqlUpdate sqlUpdate) {
        return defaultServer.execute(sqlUpdate);
    }

    public int execute(Update<?> update) {
        return defaultServer.execute(update);
    }

    public int execute(Update<?> update, Transaction transaction) {
        return defaultServer.execute(update, transaction);
    }

    public int execute(CallableSql callableSql) {
        return defaultServer.execute(callableSql);
    }

    public void externalModification(String tableName, boolean inserted, boolean updated, boolean deleted) {
        defaultServer.externalModification(tableName, inserted, updated, deleted);
    }

    public <T> T find(Class<T> beanType, Object id, Transaction transaction) {
        return defaultServer.find(beanType, id, transaction);
    }

    public void save(Object bean, Transaction transaction) throws OptimisticLockException {
        defaultServer.save(bean, transaction);
    }

    public int saveAll(Collection<?> beans, Transaction transaction) throws OptimisticLockException {
        return defaultServer.saveAll(beans, transaction);
    }

    @Nonnull
    public Set<Property> checkUniqueness(Object bean) {
        return defaultServer.checkUniqueness(bean);
    }

    @Nonnull
    public Set<Property> checkUniqueness(Object bean, Transaction transaction) {
        return defaultServer.checkUniqueness(bean, transaction);
    }

    public void markAsDirty(Object bean) {
        defaultServer.markAsDirty(bean);
    }

    public void update(Object bean) throws OptimisticLockException {
        defaultServer.update(bean);
    }

    public void update(Object bean, Transaction transaction) throws OptimisticLockException {
        defaultServer.update(bean, transaction);
    }

    public void update(Object bean, Transaction transaction, boolean deleteMissingChildren) throws OptimisticLockException {
        defaultServer.update(bean, transaction, deleteMissingChildren);
    }

    public void updateAll(Collection<?> beans) throws OptimisticLockException {
        defaultServer.updateAll(beans);
    }

    public void updateAll(Collection<?> beans, Transaction transaction) throws OptimisticLockException {
        defaultServer.updateAll(beans, transaction);
    }

    public void merge(Object bean, MergeOptions options) {
        defaultServer.merge(bean, options);
    }

    public void merge(Object bean, MergeOptions options, Transaction transaction) {
        defaultServer.merge(bean, options, transaction);
    }

    public void insert(Object bean) {
        defaultServer.insert(bean);
    }

    public void insert(Object bean, Transaction transaction) {
        defaultServer.insert(bean, transaction);
    }

    public void insertAll(Collection<?> beans) {
        defaultServer.insertAll(beans);
    }

    public void insertAll(Collection<?> beans, Transaction transaction) {
        defaultServer.insertAll(beans, transaction);
    }

    public int execute(SqlUpdate updSql, Transaction transaction) {
        return defaultServer.execute(updSql, transaction);
    }

    public int execute(CallableSql callableSql, Transaction transaction) {
        return defaultServer.execute(callableSql, transaction);
    }

    public void execute(TxScope scope, Runnable runnable) {
        defaultServer.execute(scope, runnable);
    }

    public void execute(Runnable runnable) {
        defaultServer.execute(runnable);
    }

    public <T> T executeCall(TxScope scope, Callable<T> callable) {
        return defaultServer.executeCall(scope, callable);
    }

    public <T> T executeCall(Callable<T> callable) {
        return defaultServer.executeCall(callable);
    }

    public ServerCacheManager getServerCacheManager() {
        return defaultServer.getServerCacheManager();
    }

    public BackgroundExecutor getBackgroundExecutor() {
        return defaultServer.getBackgroundExecutor();
    }

    public JsonContext json() {
        return defaultServer.json();
    }

    public DocumentStore docStore() {
        return defaultServer.docStore();
    }

    public <T> T publish(Class<T> beanType, Object id, Transaction transaction) {
        return defaultServer.publish(beanType, id, transaction);
    }

    public <T> T publish(Class<T> beanType, Object id) {
        return defaultServer.publish(beanType, id);
    }

    public <T> List<T> publish(Query<T> query, Transaction transaction) {
        return defaultServer.publish(query, transaction);
    }

    public <T> List<T> publish(Query<T> query) {
        return defaultServer.publish(query);
    }

    public <T> T draftRestore(Class<T> beanType, Object id, Transaction transaction) {
        return defaultServer.draftRestore(beanType, id, transaction);
    }

    public <T> T draftRestore(Class<T> beanType, Object id) {
        return defaultServer.draftRestore(beanType, id);
    }

    public <T> List<T> draftRestore(Query<T> query, Transaction transaction) {
        return defaultServer.draftRestore(query, transaction);
    }

    public <T> List<T> draftRestore(Query<T> query) {
        return defaultServer.draftRestore(query);
    }

    public <T> Set<String> validateQuery(Query<T> query) {
        return defaultServer.validateQuery(query);
    }
}
