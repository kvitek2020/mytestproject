package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import facades.EntitiesFacade;
import io.ebean.*;
import models.Entities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class HomeController extends Controller {

    @Inject
    EntitiesFacade entitiesFacade;

    private static final Logger LOG= LoggerFactory.getLogger(HomeController.class);

    public Result index() {
        return ok(views.html.start_page.render(entitiesFacade.getAllEntities()));
    }
    public Result sendForm() {
        JsonNode jsonNode = request().body().asJson();
        LOG.debug(jsonNode.asText());
        return ok();
    }
    public Result getObjects() {
        List<Entities> allEntities = entitiesFacade.getAllEntities();
        ArrayNode jsonNodes = Json.newArray();
        for (Entities allEntity : allEntities) {
            ObjectNode node = Json.newObject();
            node.put("name", allEntity.getName());
            node.put("type", allEntity.getType());
            node.put("css", allEntity.getCss());
            ObjectNode parametersNode = Json.newObject();
            parametersNode.put("parameters", allEntity.getParameters().getParameters().toString());
            node.put("parameters", parametersNode);
            jsonNodes.add(node);
        }
        return ok(jsonNodes);
    }

}
