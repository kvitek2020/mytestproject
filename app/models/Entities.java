package models;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Clob;

@Entity
@Table(name="ENTITIES")
public class Entities implements Serializable {

    private String name;

    private String type;

    private String css;

    @Column(name = "ID_PARAMETERS")
    private String idParameters;

    @OneToOne
    @JoinColumn(name = "ID_PARAMETERS")
    private Parameters parameters;

    public Entities() {
    }

    public Entities(String name, String type, String css, String idParameters) {
        this.name = name;
        this.type = type;
        this.css = css;
        this.idParameters = idParameters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public String getIdParameters() {
        return idParameters;
    }

    public void setIdParameters(String idParameters) {
        this.idParameters = idParameters;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }
}
