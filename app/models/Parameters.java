package models;

import javax.persistence.*;
import java.sql.Clob;

@Entity
@Table(name = "PARAMETERS")
public class Parameters {

    public Parameters() {
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    private String parameters;
    @Column(name = "labelText")
    private String labelText;


    public String getLabelText() {
        return labelText;
    }

    public void setLabelText(String labelText) {
        this.labelText = labelText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }
}
