name := """tooManyProjects"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean, LauncherJarPlugin, SbtWeb)

//topLevelDirectory := None
//mappings in (Compile, packageDoc) := Seq()

//resolvers += "Local Maven Repository" at s"file:///${Path.userHome.absolutePath}/.m2/repository"
//resolvers += "Artifactory SBT" at "http://172.16.1.37:8081/artifactory/sbt-repos"
//resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
//resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

//resolvers += "Oracle Repository" at "https://maven.oracle.com"
scalaVersion := "2.12.4"

libraryDependencies ++= Seq( jdbc , ehcache , ws , guice, evolutions) // , filters

pipelineStages := Seq(digest, gzip)

//libraryDependencies += "com.oracle.jdbc" % "ojdbc8" % "12.2.0.1"
//libraryDependencies += "com.oracle.jdbc" % "ojdbc8" % "18.3.0.0"
libraryDependencies += "org.bouncycastle" % "bcprov-jdk16" % "1.46"

//libraryDependencies += "org.postgresql" % "postgresql" % "42.2.4"
//libraryDependencies += "org.julienrf" %% "play-jsmessages" % "3.0.0"
//libraryDependencies += "org.apache.poi" % "poi" % "3.17"
//libraryDependencies += "org.apache.xmlgraphics" % "fop" % "2.3"
//libraryDependencies += "com.google.code.gson" % "gson" % "2.8.5"
//libraryDependencies += "com.openhtmltopdf" % "openhtmltopdf-jsoup-dom-converter" % "0.0.1-RC12"
//libraryDependencies += "com.openhtmltopdf" % "openhtmltopdf-pdfbox" % "0.0.1-RC12"

//libraryDependencies ++= Seq( specs2 % Test)

//libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "3.4.0" % Test
libraryDependencies += "org.seleniumhq.selenium" % "selenium-chrome-driver" % "3.1.0" % Test
//libraryDependencies += "org.seleniumhq.selenium" % "selenium-remote-driver" % "3.9.1" % Test
libraryDependencies += "org.seleniumhq.selenium" % "selenium-firefox-driver" % "3.1.0" % Test
//libraryDependencies += "org.seleniumhq.selenium" % "selenium-chromium-driver" % "3.1.0" % Test
libraryDependencies += "org.seleniumhq.selenium" % "selenium-ie-driver" % "3.1.0"
libraryDependencies += "com.h2database" % "h2" % "1.4.192"
libraryDependencies += "org.mockito" % "mockito-all" % "1.10.19" % Test
libraryDependencies += "com.typesafe.play" %% "play-mailer" % "6.0.1"
libraryDependencies += "com.typesafe.play" %% "play-mailer-guice" % "6.0.1"
//libraryDependencies += "io.ebean" % "ebean-agent" % "11.11.1" % Test

unmanagedResourceDirectories in Test +=  baseDirectory ( _ /"target/web/public/test" ).value